/**
 * @swagger
 * tags:
 *   - name: Category
 *     description: Category management API
 */

/**
 * @swagger
 * /api/v1/category:
 *   get:
 *     summary: Returns a list of categories
 *     tags: [Category]
 *     responses:
 *       200:
 *         description: A list of categories.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 success:
 *                   type: boolean
 *                 categories:
 *                   type: array
 *                   items:
 *                     type: object
 *                     properties:
 *                       _id:
 *                         type: string
 *                       name:
 *                         type: string
 *       400:
 *         description: Bad request
 */

/**
 * @swagger
 * /api/v1/category:
 *   post:
 *     summary: Create a new category
 *     tags: [Category]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *               description:
 *                 type: string
 *     responses:
 *       201:
 *         description: Created successfully.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 success:
 *                   type: boolean
 *                   example: true
 *                 category:
 *                   type: object
 *                   properties:
 *                     _id:
 *                       type: string
 *                     name:
 *                       type: string
 *       400:
 *         description: Bad request
 */

const express = require('express');
const router = express.Router();
const Category = require('../models/CategoryModel');

// GET all categories
router.get('/', async (req, res) => {
    try {
        const { page = 2, limit = 2, sortBy = 'name', order = 'asc' } = req.query;
        const sortOrder = order === 'asc' ? 1 : -1;


        const categories = await Category.find()
            .sort({ [sortBy]: sortOrder })
            .limit(Number(limit))
            .skip((Number(page) - 1) * Number(limit));

        res.status(200).json({
            success: true,
            categories
        });
    } catch (error) {
        res.status(400).json({
            success: false,
            error: error.message
        });
    }
});

// POST a new category
router.post('/', async (req, res) => {
    try {
        const category = new Category(req.body);
        await category.save();
        res.status(201).json({
            success: true,
            category
        });
    } catch (error) {
        console.log(error);
        res.status(400).json({
            success: false,
            error: error.message
        });
    }
});

module.exports = router;
