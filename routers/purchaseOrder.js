const express = require("express");
const router = express.Router();
const User = require("../models/User.js");
const jwt = require('jsonwebtoken');
const auth = require('../middleware/JwtAuthentication');
const Order = require("../models/OrderModel.js");
const ProductModel = require("../models/ProductModel.js");

router.post('/', auth, async (req, res) => {
    try {
        const token = req.header('Authorization');
        const decoded = jwt.verify(token, process.env.JWT_SECRETE);
        const user = await User.findOne({ email: decoded.email });

        const newOrder = new Order({
            userId: user._id,
            products: req.body.products,
            description: req.body.description,
            status: 'created'
        });

        const order = await newOrder.save();
        updateProducts(order.products);
        res.status(200).json({
            success: true,
            order: order
        });

    } catch (error) {
        res.status(400).json({
            success: false,
            error: error.message
        });
    }
});

const updateProducts = async (products) => {
    console.log(products);
    for (let product of products) {
        try {
            let prd = await ProductModel.findById(product.productId);
            console.log(prd);
            let total = prd.stock;
            prd.stock = total - product.quantity;
            await ProductModel.findByIdAndUpdate(product.productId, prd, { new: true, runValidators: true });

        } catch (error) {
            console.log(error);
        }

    }


}


module.exports = router;