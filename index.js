var express = require('express');
const mongoose = require('mongoose');
const colors = require('colors');
const dotenv = require('dotenv');
const cors = require('cors');
const swaggerUi = require('swagger-ui-express');
const swaggerJSDoc = require('swagger-jsdoc');

dotenv.config(); // Ensure to load the environment variables from .env file

var app = express();
app.use(cors());

app.use(express.json({
    extended: true
}));

const swaggerOptions = {
    swaggerDefinition: {
        openapi: '3.0.0',
        info: {
            title: 'Shopping Cart Api Documentation',
            version: '1.0.1',
            description: 'API documentations For Backend of Shoping cart ',
        },
        servers: [
            {
                url: `http://localhost:${process.env.PORT}`,
                description: 'Local development server',
            },
        ],
    },
    apis: ['./routers/categoryRouter.js', './routers/productRouter.js'],
};

const swaggerSpec = swaggerJSDoc(swaggerOptions);

// Serve Swagger UI at /api-docs endpoint
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

// Database connection
mongoose.connect(process.env.DATABASE_URL, { useNewUrlParser: true, useUnifiedTopology: true }); // Ensure to use the correct connection options
const db = mongoose.connection;
db.on('error', (error) => console.log(error.red));
db.once('open', () => console.log("Connected to Database".green.underline.bold));

// API's
app.get("/health", (req, res) => {
    res.send("Shopping Cart Application Running");
});

// Use the routers
app.use('/api/v1/product', require('./routers/productRouter'));
app.use('/api/v1/category', require('./routers/categoryRouter'));
app.use('/api/v1/user', require('./routers/userRouter'));
app.use('/api/v1/order', require('./routers/purchaseOrder'));

let server = app.listen(process.env.PORT, () => {
    console.log(`Server is running on http://localhost:${process.env.PORT}/`);
});
